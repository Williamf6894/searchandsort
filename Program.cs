﻿using System;
using System.Collections.Generic;

namespace Searcher
{
    class Program
    {

        private static List<ISorter> _arrayOfSorters = new List<ISorter>();
        private static List<ISearcher> _arrayOfSearchers = new List<ISearcher>();
        private Printer print = new Printer();

        public static void Main(string[] args)
        {
            int[] dataAsInt = { 1, 4, 7, 13, 4, 21, 6, 9, 2, 1, 5, 10 };
            string[] dataAsString = { "James", "David", "William", "Ann", "Alex", "Aoife", "Patrick", "Aisling", "Kate", "Andrew", "Sarah" };

            var print = new Printer();

            testSorters(dataAsInt, print);
            testSearchers(dataAsInt, 7, print);
            testSearchers(dataAsString, "Ann", print);

        }

        public static void testSorters(int[] data, Printer print) 
        {
            _arrayOfSorters.Add(new ShellSort( (int[]) data.Clone() ));
            _arrayOfSorters.Add(new BubbleSort( (int[]) data.Clone() ));
            _arrayOfSorters.Add(new InsertionSort( (int[]) data.Clone() ));

            print.printSorters(_arrayOfSorters);
        }


        public static void testSearchers(int[] data, int number, Printer print)
        {
            _arrayOfSearchers.Add(new BinarySearch(data));
            _arrayOfSearchers.Add(new SequentialSearch(data));
            print.printSearchers(_arrayOfSearchers, number);
            _arrayOfSearchers.Clear();
        }

        public static void testSearchers(string[] data, string name, Printer print)
        {
            _arrayOfSearchers.Add(new BinarySearch(data));
            _arrayOfSearchers.Add(new SequentialSearch(data));
            print.printSearchers(_arrayOfSearchers, name);
            _arrayOfSearchers.Clear();
        }

    }
}
