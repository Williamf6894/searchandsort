namespace Searcher
{
    public interface ISorter
    {
        int[] sort();

        string getName();
        int[] getData();

    }
}