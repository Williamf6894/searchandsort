namespace Searcher
{
    public class BubbleSort : ISorter
    {
        private int[] _data;

        public BubbleSort(int[] data) 
        {
            this._data = data;
        }


        public int[] sort() {
            int pass, comparison, tempValue;

            for (pass = 1; pass <= this._data.Length; pass++) 
            {
                for (comparison = 1; comparison <= this._data.Length - pass; comparison++) 
                {
                    if(this._data[comparison-1] > this._data[comparison])
                    {
                        // Swap the values around from comparison and comparison-1
                        tempValue = this._data[comparison-1];
                        this._data[comparison-1] = this._data[comparison];
                        this._data[comparison] = tempValue;

                    }
                }
            }

            return this._data;
        }

        public string getName() 
        {
            return "Bubble Sort";
        }

        public int[] getData()
        {
            return this._data;
        }
    }
}