namespace Searcher
{
    public class SequentialSearch : ISearcher
    {
        private int[] _IntData;
        private string[] _StringData;
        private bool usingStrings = false;
        private bool usingInts = false;

        public SequentialSearch(int[] intData)
        {
            this._IntData = intData;
            this.usingInts = true;
        }

        public SequentialSearch(string[] stringData)
        {
            this._StringData = stringData;
            this.usingStrings = true;
        }

        public bool search(int query)
        {
            if(!usingInts)
                return false;

            bool found = false;
            int index = 0;

            while ((!found) && (index < this._IntData.Length))
            {
                if (query == this._IntData[index]) 
                {
                    // Could log position here 
                    found = true;
                }
                index++;
            }
            return found;
        }

        public bool search(string query)
        {
            if(!usingStrings)
                return false;

            bool found = false;
            int index = 0;

            while ((!found) && (index < this._StringData.Length))
            {
                if ( query.ToLowerInvariant().Equals( this._StringData[index].ToLowerInvariant() ) ) 
                {
                    // Could log position here 
                    found = true;
                }
                index++;
            }
            return found;
        }

        public string getName() 
        {
            return "Sequential Search";
        }

        // This is a bit of a hack, might move Data to its own class.
        public int[] getIntData() 
        {
            return this._IntData;
        }
        public string[] getStringData() 
        {
            return this._StringData;
        }

    }
}