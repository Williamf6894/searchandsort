﻿using System;

namespace Searcher
{
    class ShellSort : ISorter
    {
        private int[] _data;

        public ShellSort(int[] data)
        {
            this._data = data;
        }

        public int[] sort() 
        {
            int index;
            int tempValue;
            int gap = (int) this._data.Length / 2;

            bool doneFlag = false;
            while (gap >= 1) 
            {
                doneFlag = false;
                while(doneFlag == false) 
                {
                    // If nothing was done in the last intereation leave this loop.
                    doneFlag = true;

                    for (index = 0; (index < (this._data.Length - gap)); index ++)
                    {
                        if ( this._data[index] < this._data[index + gap])
                        {

                            // Swap the values around from index and index+gap
                            tempValue = this._data[index];
                            this._data[index] = this._data[index + gap];
                            this._data[index + gap] = tempValue;

                            // Something was done, so stay in the loop.
                            doneFlag = false;
                        }
                    }
                }
                gap = (gap/2);
            }
            return this._data;
        }

        public string getName() 
        {
            return "Shell Sort";
        }

        public int[] getData()
        {
            return this._data;
        }
    }
}
