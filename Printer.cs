using System;
using System.Collections.Generic;

namespace Searcher
{
    public class Printer
    {
        public Printer(){}
        public void printSorters(List<ISorter> sorters)
        {
            foreach (var sorter in sorters)
            {
                Console.WriteLine("===========================");
                Console.WriteLine("Sorter name: " + sorter.getName());
                Console.WriteLine("Raw data: " + printData(sorter.getData()));
                Console.WriteLine("Sorted data: " + printData(sorter.sort()));
                Console.WriteLine("===========================");
            }
        }

        private string printData(int[] data)
        {
            var result = "";
            foreach (var number in data)
                result += number.ToString() + ", ";

            return result;
        }

        private string printData(string[] data)
        {
            var result = "";
            foreach (var word in data)
                result += word + ", ";

            return result;
        }

        public void printSearchers(List<ISearcher> searchers, int target)
        {
            foreach (var searcher in searchers)
            {
                Console.WriteLine("===========================");
                Console.WriteLine("Searcher name: " + searcher.getName());
                Console.WriteLine("Raw data: " + printData(searcher.getIntData()));
                Console.WriteLine("Found " + target + ": " + searcher.search(target));
                Console.WriteLine("===========================");
            }
        }
        
        public void printSearchers(List<ISearcher> searchers, string target)
        {
            foreach (var searcher in searchers)
            {
                Console.WriteLine("===========================");
                Console.WriteLine("Searcher name: " + searcher.getName());
                Console.WriteLine("Raw data: " + printData(searcher.getStringData()));
                Console.WriteLine("Found " + target + ": " + searcher.search(target));
                Console.WriteLine("===========================");
            }
        }
    }
}