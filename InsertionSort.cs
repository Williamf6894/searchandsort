namespace Searcher
{
    public class InsertionSort : ISorter
    {
        private int[] _data;

        public InsertionSort(int[] data)
        {
            this._data = data;
        }

        public int[] sort() 
        {
            int primary = 1;
            int secondary = 1;
            int dataValue;

            for (primary = 1; primary < this._data.Length; primary++) 
            {
                // Temp stores current
                dataValue = this._data[primary];

                secondary = primary;

                // Shuffle everything down until value can be inserted
                while( (secondary > 0) && (this._data[secondary - 1] > dataValue) ) 
                {
                    this._data[secondary] = this._data[secondary - 1];
                    secondary--;
                }
                this._data[secondary] = dataValue;
            }

            return this._data;
        }

        public string getName() 
        {
            return "Insertion Sort";
        }

        public int[] getData()
        {
            return this._data;
        }

    }
}