using System;
namespace Searcher
{
    public class BinarySearch : ISearcher
    {
        private int[] _IntData;
        private string[] _StringData;
        private bool usingStrings = false;
        private bool usingInts = false;

        public BinarySearch(int[] intData)
        {
            this._IntData = intData;
            this.usingInts = true;
        }

        public BinarySearch(string[] stringData)
        {
            this._StringData = stringData;
            this.usingStrings = true;
        }

        public bool search(string query)
        {
            if(!usingStrings) {
                return false;
            }
            // Assumes sorted
            int first = 0;
            int last = this._StringData.Length - 1;
            int middle = (last - first) / 2;
            bool found = false;

            while( first <= last && !found)
            {
                if( this._StringData[middle].ToLower().Equals(query.ToLower()) )
                    found = true;

                else if( this._StringData[middle].ToLower().CompareTo(query.ToLower()) > 0 )
                    last = middle - 1;
                    
                else
                    first = middle + 1;

                middle = (last - first) / 2;
            }
            return found;
        }

        public bool search(int query)
        {
            if(!usingInts) {
                return false;
            }

            // Assumes sorted
            // Need to specify if asc or desc
            int first = 0;
            int last = this._IntData.Length - 1;
            int middle = (last - first) / 2;

            bool found = false;

            while( first <= last && !found)
            {
                if( this._IntData[middle] == query )
                    found = true;

                else if( this._IntData[middle] > query )
                    last = middle - 1;
                    
                else
                    first = middle + 1;

                middle = (last - first) / 2;
            }
            return found;
        }

        public string getName()
        {
            return "Binary Search";
        }
        
        public int[] getIntData() 
        {
            return this._IntData;
        }
        public string[] getStringData() 
        {
            return this._StringData;
        }
    }
}