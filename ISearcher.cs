namespace Searcher
{
    public interface ISearcher
    {
        bool search(string query);
        bool search(int query);
        string getName();
        string[] getStringData();
        int[] getIntData();
    }
}